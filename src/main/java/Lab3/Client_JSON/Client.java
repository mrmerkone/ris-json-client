package Lab3.Client_JSON;


import com.thetransactioncompany.jsonrpc2.client.*;

// The Base package for representing JSON-RPC 2.0 messages
import com.thetransactioncompany.jsonrpc2.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
// For creating URLs
import java.net.*;
import java.util.HashMap;
import java.util.Map;

public class Client {
	
	private static String fileName = "Multiply.java";
	
	public static void main(String[] args) throws IOException {
		URL serverURL = null;
		try {
			serverURL = new URL("http://localhost:8080");
		} catch (MalformedURLException e) {
			// Обработка ошибки
		}
		// Инициализация соединения
		JSONRPC2Session mySession = new JSONRPC2Session(serverURL);
		mySession.getOptions().setAllowedResponseContentTypes(null);
		// Формируем getDate запрос

		String method = "process";
		int requestID = 0;

		Map<String, Object> content = new HashMap<String, Object>();

		content.put("code", readFile(fileName));

		JSONRPC2Request request = new JSONRPC2Request(method, content, requestID);
		// Отправляем запрос
		JSONRPC2Response response = null;
		try {
			response = mySession.send(request);
		} catch (JSONRPC2SessionException e) {
			System.err.println("ansErr: " + e.getMessage());
			// Обработка исключения
		}
		// Вывод ответа или ошибки
		if (response.indicatesSuccess())			
			System.out.println(fileName + " stdout:\n" + response.getResult());
		else
			System.out.println(response.getError().getMessage());

	}

	/**
	 * Функция считывания текста файла в строку
	 * 
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	private static Object readFile(String fileName) throws IOException {
		BufferedReader fin = new BufferedReader(
				new InputStreamReader(new FileInputStream("src/main/java/" + fileName), "UTF-8"));

		String str;
		String content = "";
		while ((str = fin.readLine()) != null)
			content = content + str + "\n";
		fin.close();

		return content;
	}
}